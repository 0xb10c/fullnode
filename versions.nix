# order matters: last item in every list becomes "latest" tag
rec {
  bitcoin = [
    "0.18.0"
    "0.18.1"
    "0.19.0.1"
    "0.19.1"
  ];
  bitcoin-abc = [
    "0.21.0"
    "0.21.1"
    "0.21.2"
    "0.21.3"
  ];
  bitcoin-gold = [
    "0.15.2"
    "0.17.1"
  ];
  bitcoin-sv = [
    "1.0.0"
    "1.0.1"
    "1.0.2"
  ];
  cardano = [
    "3.0.3"
    "3.1.0"
    "3.2.0"
  ];
  cardano-cmfork = cardano;
  elements = [
    "0.18.1.3"
    "0.18.1.4"
  ];
  ethereum-parity = [
    "2.6.8"
    "2.5.13"
    "2.7.2"
  ];
  grin = [
    "3.1.0"
    "3.1.1"
  ];
  monero = [
    "0.15.0.5"
  ];
  omnicore = [
    "0.7.1"
    "0.8.0"
    "0.8.1"
  ];
  ripple = [
    "1.4.0"
  ];
}
